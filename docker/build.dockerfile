#
#     QuantLET - an event driven framework for large scale real-time analytics
#
#     Copyright (C) 2006 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#     '
#     '
FROM continuumio/miniconda3:4.7.12
RUN apt-get update
RUN apt-get -y install make g++ libsnappy-dev
RUN pip install --upgrade pip
RUN mkdir -p /app/quantlet
WORKDIR /app
## cache dependencies
RUN pip install jfaleiro.setup_headers
COPY requirements.txt setup.py README.md /app/
COPY quantlet/_version.py quantlet/__init__.py /app/quantlet/
RUN pip install -r requirements.txt
COPY requirements-extras.txt /app
RUN pip install -r requirements-extras.txt
## post dependencies install steps
COPY . /app
RUN python setup.py install
