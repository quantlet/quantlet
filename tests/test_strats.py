#
#     QuantLET - an event driven framework for large scale real-time analytics
#
#     Copyright (C) 2006 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
def test_bands():
    from quantlet.strats.bands import bollinger


def test_filter():
    from quantlet.strats.filter import cma


def test_mathfin():
    from quantlet.strats.mathfin import multiple_period_return


def test_plot():
    from quantlet.strats.plot import animate


def test_rw():
    from quantlet.strats.rw import random_uniform
