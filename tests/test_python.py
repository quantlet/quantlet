#
#     QuantLET - an event driven framework for large scale real-time analytics
#
#     Copyright (C) 2006 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import collections


def test_class_decorators():
    class S(object):
        def __init__(self, function):
            self.function = function

        def __rrshift__(self, other):
            return self.function(other)

        def __call__(self, *args, **kwargs):
            return S(lambda x: self.function(x, *args, **kwargs))

    @S
    def a(iter):
        for i in iter:
            print(i)

    (1, 2, 3) >> a

    @S
    def b(iter):
        for i in iter:
            yield i

    (1, 2, 3) >> b >> a


def test_classes_calls():
    class S(object):
        pass

    print(S)
    print(S())


def test_decorators_as_method():
    def _no_exceptions(o, f):
        def wrapper():
            try:
                print("in %s" % o.label)
                o.valid = True
                f()
            except TypeError as _:
                print("got %s error" % o.label)
                o.valid = False

        return wrapper

    class S(object):
        def _raise(self, *args, **kwargs):
            raise Exception()

        def _get_val(self):
            try:
                print("in2 %s" % self.label)
                self.trace = None
                return self.f()
            except TypeError as e:
                self.trace = e
                print("got2 %s error" % self.label)
                return None

        def _set_val(self, val):
            self.f = lambda: val

        val = property(_get_val, _set_val, _raise, "value")

        def _is_valid(self):
            return self.trace is None

        valid = property(_is_valid, _raise, _raise, "is_valid")

        def __init__(self, value=None, l=""):
            if hasattr(value, "__call__"):
                self.f = value
            else:
                self.f = lambda: value
            self.label = l
            self.trace = None

        def __add__(self, other):
            return S(
                lambda: self.val +
                other.val, "[%s+%s]" % (self.label, other.label)
            )

        def __call__(self, *args, **kwargs):
            return self.f()

    A = S(l="A")
    B = S(l="B")
    C = A + B
    assert A.val is None and A.valid
    assert B.val is None and B.valid
    print("start...")
    assert C.val is None and not C.valid
    A.val = 1
    assert A.val == 1 and A.valid
    assert B.val is None and B.valid
    assert C.val is None and not C.valid
    B.val = 2
    assert A.val == 1 and A.valid
    assert B.val == 2 and B.valid
    assert C.val == 3 and C.valid


def test_decorators():
    """
    behaviors of decorators
    """

    def d(f):
        def decorator(a, b):
            try:
                print("d(before)")
                f(a, b)
                print("d(after)")
            except BaseException:
                pass

        return decorator

    @d
    def a(a, b):
        print("a()")

    a(1, 2)


def test_dict_to_namedtuple():
    d = dict(a=1, b=2, c=3)
    Type = collections.namedtuple("Type", "a b c")
    t = Type(**d)
    assert 1 == t.a
    assert 2 == t.b
    assert 3 == t.c
