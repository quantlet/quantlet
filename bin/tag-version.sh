scmversion tag-version
export POETRY_VERSION=`scmversion version` && envsubst < pyproject.template.toml >| pyproject.toml
rm dist/*.tar.gz
poetry build --format sdist
tar -xvf dist/*.tar.gz --wildcards --no-anchored '*/setup.py' --strip=1
