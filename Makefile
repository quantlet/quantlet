#
#     QuantLET - an event driven framework for large scale real-time analytics
#
#     Copyright (C) 2006 Jorge M. Faleiro Jr.
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
.PHONY: test coverage deploy.staging deploy.production deploy headers \
clean

test.agents:
	poetry install -E agents -E tests
	python -m pytest tests/test_agents.py --durations=0  -vv

test.core:
	poetry install -E tests
	python -m pytest tests/test_python.py tests/test_core.py --durations=0  -vv

test.ml:
	poetry install -E ml -E tests
	python -m pytest -v tests/test_ml.py --durations=0  -vv

test.reactives:
	poetry install -E reactives -E tests
	python -m pytest tests/test_reactives.py --durations=0  -vv

test.strats:
	poetry install -E strats -E tests
	python -m pytest -v tests/test_strats.py --durations=0  -vv

test.streaming:
	poetry install -E streaming -E tests
	python -m pytest tests/test_streaming.py --durations=0  -vv

test.timeseries:
	poetry install -E timeseries -E tests
	python -m pytest tests/test_timeseries.py --durations=0  -vv

headers:
	./setup.py adjust_license_headers
coverage:
	poetry install -E timeseries -E reactives -E agents -E streaming -E strats -E ml -E coverage
	coverage run --omit='tests/*' --source quantlet -m pytest
	coverage report -m
	coverage html -d build/coverage/html

prepare:
	poetry install -E coverage -E tests -E interactive-dev
clean:
	rm -rf .tox build .pytest_cache
version:
	scmversion tag-version
submodule.add:
	git submodule add --force --name quantlet-core ../quantlet-core.git .submodules/quantlet-core
	git submodule add --force --name quantlet-streaming ../quantlet-streaming.git .submodules/quantlet-streaming
submodule.rm:
	git submodule deinit --force .submodules/quantlet-core
	git rm .submodules/quantlet-core
	git submodule deinit --force .submodules/quantlet-streaming
	git rm .submodules/quantlet-streaming
submodule.update:
	git submodule update --init --recursive
